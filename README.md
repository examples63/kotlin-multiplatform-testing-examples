# Kotlin Multiplatform example (assignment task)

**Author:** [Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

## Techstack :
  
  Kotlin Multiplatform Mobile (KMM) + Compose Multiplatform UI (99% shared code)
  
  Download [Android Studio stable](https://developer.android.com/studio)
  
  Install [Kotlin Multiplatform plugin](https://plugins.jetbrains.com/plugin/14936-kotlin-multiplatform) to Android Studio
 
 Follow [Set up KMM environment﻿](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-setup.html) guide


Try to [Run your application](https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-multiplatform-create-first-app.html#run-your-application)

**Video recording and playing** is done via. native Android and iOS libs, see [Using Native code with Common code](https://kotlinlang.org/docs/multiplatform-expect-actual.html)

## Best practice :

Keep **Android Studio up-to-date** ( "Android Studio" -> "Check for updates...")

Keep **0 Warinings in the project !** ( "Android Srudio" -> "Code" -> "Inspect code...")

Keep all **external libraries up-to-date** ( In Android Studio, open ["./gradle/libs.versions.toml"](https://github.com/git-me-an-answer/meemori/blob/development/gradle/libs.versions.toml) and resolve all out-of-date version warnings + Sync Gradle + rebuild for both Android and iOS) 

**Ask users to give reviews on AppStore, otherwise the Algorithm will ignore your app.**

**Android Hardware compatibility testing** on [**Firebase TestLab**](https://console.firebase.google.com/project/meemori-f5541/testlab/histories) - using AI to test your app on various devices. In Android world it is absolutelly necessary to do these tests before every release or update of the app.

**Crash rate <= 2,5%** : I have set up email and in-app notifications (you have to install Firebase app on your smartphone and login to project). Biggest mistake ever is to mismanage crash-rate, all users will leave.

## KMM documentation

[KMM project structure](https://kotlinlang.org/docs/multiplatform-discover-project.html)

[Multiplatform resources﻿](https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-images-resources.html)

[Ktor doc](https://ktor.io/docs/client-supported-platforms.html)

[Kotlin Multiplatform Dependency Injection](https://insert-koin.io/docs/reference/koin-mp/kmp)

[Compilation settings](https://kotlinlang.org/docs/multiplatform-configure-compilations.html)

[Compose Multiplatform (UI) Lifecycle](https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-lifecycle.html)

[Koin for DI](https://insert-koin.io/docs/quickstart/kotlin/)

[ViewModel](https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-viewmodel.html)

[Advanced KMM project structure﻿](https://kotlinlang.org/docs/multiplatform-advanced-project-structure.html)

[Core KMM Know-how list](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-wrap-up.html)

[Adding dependencies on multiplatform libraries](https://kotlinlang.org/docs/multiplatform-add-dependencies.html)

[Using Native code with Common code](https://kotlinlang.org/docs/multiplatform-expect-actual.html)

[Kotlin Multiplatform samples﻿](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-samples.html)

[Awesome Kotlin Multiplatform Libraries](https://github.com/terrakok/kmp-awesome)

## Used 3rd party libs + other resources :

[Firebase Kotlin SDK](https://github.com/GitLiveApp/firebase-kotlin-sdk)

[Ktor, kotlinx.serialization, kotlinx.coroutines](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-upgrade-app.html)

[BD, SQLDelight, Koin for DI](https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-ktor-sqldelight.html)

[Tiamat for navigation](https://github.com/ComposeGears/Tiamat) [(currently native experimental, candidate for refactoring)](https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-navigation-routing.html#limitations)

[Koin for DI](https://insert-koin.io/docs/setup/koin/)


# WBPO Assignment task

## Details

### UI:

(2 screens)

- registration
- list of users

landscape and portrait mode

XML-based components. Do not use Jetpack Compose


### API:

Use fake data from [reqres.in](https://reqres.in/)


### Registration screen:

API: [https://reqres.in/api/register](https://reqres.in/api/register)

- launched for the first time, or when there is no successful registration yet
- loading and errors should be displayed + the option to repeat after failed
- After successful registration, the app should display a User list


### User list screen:

API: [https://reqres.in/api/users](https://reqres.in/api/users?page=1&per_page=5)

- pagination
-  row should contain a Follow/Unfollow button that is rendered based on the
saved data about the followed users.
- loading, or any errors, should be displayed + option to repeat the failed action
